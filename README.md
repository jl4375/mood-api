I ran this in a virtual enviornment, to activate the virtual enviornment, type "activate" in the terminal while in the Scripts directory. 
Also make sure that the neccessay frameworks/libraries are installed. You can install them with: pip install flask flask_sqlalchemy flask_login flask_wtf wtforms email_validator.
After starting the virtual enviornment, run "flask shell" in the home directory, and run "db.drop_all()", "db.create_all()" and then "exit()".
This will create the database. After that the program can be started with python app.py. 
You can manually change the date of the current day to test the project by commenting out line 57 on app.py and uncommenting line 58 and changing the date to the desired date.
It should be noted that I did this project in Windows.
If this were a a production application instead, then I would add more to the UI/UX like with CSS or even JavaScript.
If there were more kinds of data, then I would need to probably make more tables and create a larger database.
