from flask import Flask, render_template, url_for, redirect
from flask_sqlalchemy import SQLAlchemy
from flask_login import UserMixin, login_user, LoginManager, login_required, logout_user, current_user
from flask_wtf import FlaskForm
from wtforms import Form, validators, StringField, SubmitField
from datetime import date
import datetime

db = SQLAlchemy()
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI']= 'sqlite:///database.db'
app.config['SECRET_KEY'] = 'thisisakey'
db.init_app(app)
login_manager =  LoginManager()
login_manager.init_app(app)
login_manager.login_view="login"
@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


class Mood(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    mood = db.Column(db.String(20), nullable=False)
    date = db.Column(db.String(10), nullable=False)
    username = db.Column(db.String(20), nullable=False)

class MoodForm(FlaskForm):
    mood = StringField(validators=[validators.DataRequired(), validators.Length(min=2, max=20)], render_kw={"placeholder": "Mood"})
    submit = SubmitField("Confirm Mood")


class User(db.Model, UserMixin):
      id = db.Column(db.Integer, primary_key=True)
      username = db.Column(db.String(20), nullable=False, unique=True)

class LoginForm(FlaskForm):
      username = StringField(validators=[validators.DataRequired(), validators.Length(min=4, max=20)], render_kw={"placeholder": "Username"})
      submit = SubmitField("Login")

class RegistrationForm(FlaskForm):
      username = StringField(validators=[validators.DataRequired(), validators.Length(min=4, max=20)], render_kw={"placeholder": "Username"})
      submit = SubmitField("Register")

      def validate_username(self, username):
          existing_username = User.query.filter_by(username=username.data).first()

          if existing_username:
              raise validators.ValidationError("that username already exists")


@app.route("/mood", methods=["GET", "POST"])
@login_required
def mood():
    form = MoodForm()
    if form.validate_on_submit():
        today = str(date.today())
        #today = "2022-12-02"
        submittable = Mood.query.filter_by(date=today, username=current_user.username).first()
        if not submittable:
            new_mood = Mood(mood=form.mood.data, date=today, username=current_user.username)
            db.session.add(new_mood)
            db.session.commit()

    streak = 0
    year = date.today().year
    month = date.today().month
    day =date.today().day
    c_epoch=datetime.datetime(year,month,day,0,0).timestamp()
    while(True):
        c_epoch-=86400
        dt = str(datetime.datetime.fromtimestamp(c_epoch)).split(" ")[0]
        query = Mood.query.filter_by(username=current_user.username, date=dt).first()
        if query:
            streak += 1
        else:
            break
    if(Mood.query.filter_by(username=current_user.username, date=date.today()).first()):
        streak+=1


    moods= Mood.query.filter_by(username=current_user.username)
    return render_template("mood.html", form = form, moods = moods, streak=streak)


@app.route("/login", methods=["GET", "POST"])
@app.route("/", methods=["GET", "POST"])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user:
            login_user(user)
            return redirect(url_for("mood"))
    return render_template("login.html", form = form)


@app.route("/logout", methods=["GET", "POST"])
@login_required
def logout():
    logout_user()
    return redirect(url_for("login"))


@app.route("/register", methods=["GET", "POST"])
def register():
    form = RegistrationForm()
    if form.validate_on_submit():
        new_user = User(username=form.username.data)
        db.session.add(new_user)
        db.session.commit()
        return redirect(url_for("login"))
    return render_template("register.html", form = form)


if __name__ == "__main__":
    app.run()